#include "mainwindow.h"
#include "lib/layout_macros.h"
#include "lib/painter_ext.h"

#include <QAction>
#include <QDebug>
#include <QWindow>

#include <QBoxLayout>

#include <QButtonGroup>
#include <QRadioButton>
#include <QSpinBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // Make title bar draggable.
    resize(320, 240);

    auto main = this;
    auto do_update = [this]() { update(); };

    {main__central_c_l(QWidget, QHBoxLayout);
        {l__l(QVBoxLayout);
            {l__w(QSpinBox);
                _lines = w;
                w->setMinimum(1);
                connect(w, &QSpinBox::valueChanged, this, do_update);
            }
            append_stretch();
        }
        {l__l(QVBoxLayout);
            _color = new QButtonGroup(this);
            {l__w(QRadioButton);
                w->setText(tr("Red"));
                _color->addButton(w, Qt::red);
                connect(w, &QRadioButton::clicked, this, do_update);
            }
            {l__w(QRadioButton);
                w->setText(tr("Green"));
                _color->addButton(w, Qt::green);
                connect(w, &QRadioButton::clicked, this, do_update);
            }
            {l__w(QRadioButton);
                w->setText(tr("Blue"));
                _color->addButton(w, Qt::blue);
                connect(w, &QRadioButton::clicked, this, do_update);
            }
            const auto buttons = _color->buttons();
            buttons[0]->setChecked(true);
            for (auto * button : buttons) {
                button->setStyleSheet("color: white");
            }

            append_stretch();
        }
        append_stretch();
    }

    _fullscreen = new QAction(tr("Full Screen"), this);
    _fullscreen->setShortcut(QKeySequence(tr("F11", "Full Screen")));

    connect(_fullscreen, &QAction::triggered, this, [this] {
        auto win = windowHandle();
        assert(win);
        if (win->windowStates() & Qt::WindowFullScreen) {
            win->setWindowState(Qt::WindowNoState);
        } else {
            win->setWindowState(Qt::WindowFullScreen);
        }
    });
    addAction(_fullscreen);
}

MainWindow::~MainWindow()
{
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    qreal dpr = devicePixelRatio();
//    assert(dpr == (qreal)(int)dpr);

    auto w = width() * (int)dpr;
    auto h = height() * (int)dpr;
    auto size = QSize(w, h);

    if (_img.size() != size) {
        _img = QImage(size, QImage::Format_RGB32);
    }

    _img.fill(Qt::black);

    auto nline = _lines->value();

    auto checked_id = _color->checkedId();
    assert(checked_id != -1);
    auto fill = (Qt::GlobalColor) checked_id;

    {
        auto region = GridRect(0, (h - nline) / 2, w, nline);

        auto p = QPainter(&_img);
        p.fillRect(region, fill);
    }

    {
        auto p = QPainter(this);
        p.drawImage(rect(), _img);
    }

    // skip: QMainWindow::paintEvent(event);
}
