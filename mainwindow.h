#pragma once

#include <QMainWindow>
#include <QImage>

class QAction;
class QSpinBox;
class QButtonGroup;

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QSpinBox * _lines;
    QButtonGroup * _color;
    QImage _img;

    QAction * _fullscreen;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

// impl QWidget
    void paintEvent(QPaintEvent *event) override;
};
